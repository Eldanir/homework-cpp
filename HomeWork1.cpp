﻿#include <iostream>

using namespace std;

class Animal
{

public:
	virtual void Voice()
	{
		cout << "text \n";
	}
	virtual ~Animal () {}
};

class Cat : public Animal
{

public:
	virtual void Voice() override
	{
		cout << "Meow! \n";
	}
};

class Dog : public Animal
{

public:
	virtual void Voice() override
	{
		cout << "Woof! \n";
	}
};

class Duck : public Animal
{

public:
	virtual void Voice() override
	{
		cout << "Quack! \n";
	}
};

int main()
{
	Animal* Animals[3];
	Animals[0] = new Dog;
	Animals[1] = new Cat;
	Animals[2] = new Duck;

	for (int i = 0; i < 3; i++)
	{
		Animals[i]->Voice();
		delete Animals[i];
	}
}